/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Button,TextInput} from 'react-native';
import networkmanager from "./networkmanagerNativeModule"

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
        data:"",
        urlentered:"http://jsonplaceholder.typicode.com/users/1",
        default:"http://jsonplaceholder.typicode.com/users/1"
    }
    // instacapturemodule.welcomeToast("welcome to my native module")
  }
  getData = () =>{
    networkmanager.exampleMethod(this.state.urlentered, function(error, msg) {
            let downloadedPath = msg.response
            console.log("Success!", downloadedPath);
            this.setState({data: JSON.stringify(msg.response)})
        }.bind(this));
    

    }



  render() {
    return (

    <View style={styles.container}>
       <Text style={styles.bggray}>##########get data############</Text>
       <Text style={styles.bgred}>####by default you can get data from http://jsonplaceholder.typicode.com/users/1#############</Text>
       <Text style={styles.bggray}>or you can enter url in input </Text>
      
      <View>
      <TextInput style={styles.input}
              onChangeText={(text) => this.setState({ urlentered: text })}
              value={this.state.urlentered}
              />
           <Button style={styles.button}  title="get data" onPress={this.getData.bind(this)} />
      </View>
      <View >
        <Text style={styles.resultbg}>{this.state.data}</Text>
      </View>

    </View>
    );
  }
}

const styles = StyleSheet.create({
   resultbg:{
    backgroundColor: '#00cc00',
  },
  bggray:{
    backgroundColor: '#999999',
  },
  bgred:{
    backgroundColor: '#ff0000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  input: {
        borderColor: 'gray',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderWidth: 1,
        marginLeft: 10,
        marginRight: 10,
        height: 40,
    },
    button: {
        height: 40,
    },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
