//  Created by react-native-create-bridge

import { NativeModules } from 'react-native'

const { networkmanager } = NativeModules

export default {
  exampleMethod (errorCallback,successCallback) {
    return networkmanager.exampleMethod(errorCallback,successCallback)
  },

  EXAMPLE_CONSTANT: networkmanager.EXAMPLE_CONSTANT
}
