//  Created by react-native-create-bridge

import Foundation
import IBGxNetworkManager

@objc(networkmanager)
class networkmanager : NSObject {
  // Export constants to use in your native module
//  override func constantsToExport() -> [String : Any]! {
//    return ["EXAMPLE_CONSTANT": "example"]
//  }

  // Implement methods that you want to export to the native module
  @objc func exampleMethod(_ fileUrl: String, callback: @escaping RCTResponseSenderBlock) {
    // write method here
    // IBGxNetworkManager.get("https://jsonplaceholder.typicode.com/posts")

    let manager = IBGxNetworkManager()
    if let urlString = URL(string: fileUrl){
      manager.get(urlString, parameters: nil) { (data, err) in
        print( data!)
//        self.jsonToString(json: data)
        callback([NSNull(), [
          "response": data!
          ]])
      }
    }
}
//  func jsonToString(json: Any){
//    do {
//      let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
//      let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
//      print(convertedString ?? "defaultvalue")
//    } catch let myJSONError {
//      print(myJSONError)
//    }
//
//  }
}
